#!/bin/bash

#script suicida p/ rc.local

#Adicionar shell zsh no usuário, correção da criação do usuário padrão pelo Calamares
sed -i 's/bash/zsh/' /etc/passwd

#verificar virtualbox
vb=$(lspci | grep -i virtualbox)
#verificar vmware
vw=$(lspci | grep -i vmware)

#se não for virtualbox desinstalar:
if [ -z "$vb" ]; then
	apt remove --purge virtualbox-guest*
fi

#se não for vmware desinstalar 
if [ -z "$vw"]; then
	apt remove --purge open-vm-tools*
fi

#desabilitar o repo do calamares
sed -i '/calamares/{s/^/#/}' /etc/apt/sources.list.d/duzeru.list


rm /postinst-root.sh
sed -i '/postinst-root.sh/d' /etc/rc.local

