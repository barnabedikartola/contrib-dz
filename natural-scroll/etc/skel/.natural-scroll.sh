#!/bin/sh
syncl=$(synclient | grep VertScrollDelt | cut -d "=" -f 2 | sed 's/ /-/')

synclient VertScrollDelta=$syncl
